import React from 'react'
import { Route, Routes } from 'react-router-dom';
import { Box } from '@mui/material';

//[RESET CSS]
import './App.css'

//[COMPONENTS]
import Home from './pages/Home';
import ExerciceDetail from './pages/ExerciceDetail';
import Navbar from './components/Navbar';
import Footer from './components/Footer';


function App() {
    return (
        <Box width='400px' sx={{ width: { xl: '14488px' } }} m="auto">
            <Navbar />
            <Routes>
                <Route path='/' element={<Home />} />
                <Route path='/exercise/:id' element={<ExerciceDetail />} />
            </Routes>
            <Footer />
        </Box>
    )
}

export default App