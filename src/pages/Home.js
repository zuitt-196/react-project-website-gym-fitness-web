import React, { useState } from 'react'
import { Box } from '@mui/material';
import { Search } from '@mui/icons-material';

// [IMPORT COMPONENT
import HeroBanner from '../components/HeroBanner';
import SearchExercise from '../components/SearchExercises';
import Exercise from '../components/Exercise';


const Home = () => {
  // we define these state because they all uses to other componenmt to avoid rudantdant like  Exercise componnets 
  const [bodyPart, setBodyPart] = useState('all');
  const [exsercise, setExsercise] = useState([]);


  return (
    <Box>
      <HeroBanner />
      <SearchExercise setExsercise={setExsercise} setBodyPart={setBodyPart} bodyPart={bodyPart} />
      <Exercise
        setExsercise={setExsercise} exsercise={exsercise} bodyPart={bodyPart}

      />
    </Box>
  )
}

export default Home
