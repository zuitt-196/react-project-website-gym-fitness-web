import React, { useEffect, useState } from 'react';
import { Box, Button, Stack, TextField, Typography } from '@mui/material';
import HorizontalScrollbar from './HorizontalScrollbar'

import { exerciseOptions, fetchData } from '../utils/fetchData';

const SearchExercise = ({ exsercise, bodyPart, setBodyPart }) => {
    const [search, setSearch] = useState('');
    const [exercices, setExercices] = useState([]);
    const [bodyParts, setBodyparts] = useState([])


    useEffect(() => {
        const fetchExercisesData = async () => {
            const bodyPartsData = await fetchData('https://exercisedb.p.rapidapi.com/exercises/bodyPartList', exerciseOptions);
            console.log(bodyPartsData)
            setBodyparts(['all', ...bodyPartsData])

        }
        fetchExercisesData();
    }, [])


    // Handle search function 
    const handleSearch = async () => {
        if (search) {
            console.log('Search')
            const exercisesData = await fetchData('https://exercisedb.p.rapidapi.com/exercises', exerciseOptions);
            console.log(exercisesData)

            // Search the list of exercisesData 
            const SearchedExercises = exercisesData.filter(item => item.name.toLowerCase().includes(search)
                || item.target.toLowerCase().includes(search)
                || item.equipment.toLowerCase().includes(search)
                || item.bodyPart.toLowerCase().includes(search)
            );

            setSearch('');
            setExercices(SearchedExercises);

        }

    }

    return (
        <Stack mt="37px" p="23px" >
            <Typography fontWeight={700} sx={
                {
                    fontSize: {
                        lg: '44px', xs: '30px'
                    }
                }
            } mb='50px' textAlign="center">
                Awsesome Exercise You <br /> should know
            </Typography>

            <Box position="relative" mb="72px">
                <TextField
                    sx={{
                        input: {
                            fontWeight: '700',
                            border: 'none',
                            borderRadius: '5px',
                        },
                        width: {
                            lg: '1170px', xs: '350px'
                        },
                        backgroundColor: "white",
                        borderRadius: "40px"
                    }}
                    height='76px'
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    placeholder="Search Exercise"
                    type='text'
                />
                <Button className="search-btn" sx={
                    {
                        bgcolor: '#FF2625', color: '#fff', textTransform: 'none',
                        width: { lg: '175px', xs: '80px' },
                        fontSize: { lg: '20px', xs: '14px' },
                        height: '56px',
                        right: '0'
                    }

                } onClick={handleSearch}>
                    Search
                </Button>
            </Box>
            <Box sx={{ position: 'relative', width: '100%', p: '20px' }}>
                <HorizontalScrollbar data={bodyParts} bodyPart={bodyPart} setBodyPart={setBodyPart} />
            </Box>


        </Stack >

    )
}

export default SearchExercise
