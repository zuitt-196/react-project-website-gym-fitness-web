
import React, { useContext } from 'react';
// import { ScrollMenu, VisibilityContext } from "react-horizontal-scrolling-menu";
// import HorizontalScroll from 'react-scroll-horizontal'
import { Swiper, SwiperSlide } from 'swiper/react';
import { FreeMode } from "swiper"
import 'swiper/css';
import "swiper/css/free-mode";
import 'bootstrap/dist/css/bootstrap.min.css';


import { Box, Typography } from '@mui/material';

// import ExerciseCard from './ExerciseCard';
import BodyPart from './BodyPart';
import RightArrowIcon from '../assests/icons/right-arrow.png';
import LeftArrowIcon from '../assests/icons/left-arrow.png';

// const LeftArrow = () => {
//     const { scrollPrev } = useContext(VisibilityContext);

//     return (
//         <Typography onClick={() => scrollPrev()} className="right-arrow">
//             <img src={LeftArrowIcon} alt="right-arrow" />
//         </Typography>
//     );
// };

// const RightArrow = () => {
//     const { scrollNext } = useContext(VisibilityContext);

//     return (
//         <Typography onClick={() => scrollNext()} className="left-arrow">
//             <img src={RightArrowIcon} alt="right-arrow" />
//         </Typography>
//     );
// };

const HorizontalScrollbar = ({ data, setBodyPart, bodyPart }) => (
    <div>
        <Swiper
            freeMode={true}
            grabCursor={true}
            modules={[FreeMode]}
            className="mySwipper"
            slidesPerView={5}
            spaceBetween={30}
        >
            {data.map((item) => (
                <SwiperSlide
                    key={item.id || item}
                    itemId={item.id || item}
                    title={item.id || item}
                    m="0 40px"
                >
                    {<BodyPart item={item} setBodyPart={setBodyPart} bodyPart={bodyPart} />}
                </SwiperSlide>
            ))}
        </Swiper>
    </div>
);

export default HorizontalScrollbar;