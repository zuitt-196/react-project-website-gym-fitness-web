import React from 'react'
import { Box, Stack, Typography, Button } from '@mui/material';

// IMPORT HERO BANNER
import HeroBannerImage from '../assests/images/banner.png'

const HeroBanner = () => {
    return (

        <Box sx={{ mt: { lg: '212px', xs: '70px' }, ml: { sm: '50px' } }} position="relative" p="20px">
            <Typography color="#FF2625" fontWeight='600' fontSize='26px'>
                Fitnees Club
            </Typography>
            <Typography fontWeight='700' sx={
                {
                    fontSize: {
                        lg: '44px', xs: '40px'
                    }
                }

            } mb='23px' mt='30px'>
                Sweet, Smile <br /> and repeat
            </Typography>
            <Typography fontSize='23px' lineHeight='35px' mb={4}>
                Check out the most  effective exercices<br />
            </Typography>
            <Button variant="contained" color="error" href="#exercices" sx={{ backgroundColor: "#ff26525" }}>
                Explore Exercises
            </Button>
            <Typography fontWeight={600}
                color='#ff2625'
                sx={
                    {
                        opacity: 0.1,
                        display: {
                            lg: 'block', xs: 'none'
                        }

                    }

                } fontSize={200} >
                Exercises
            </Typography>

            <img src={HeroBannerImage} alt='banner' className="hero-banner-img" />
        </Box>



    )
}


export default HeroBanner


