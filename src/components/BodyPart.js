import React from 'react'
import { Stack, Typography } from '@mui/material';

import Icon from '../assests/icons/gym.png';
import { green } from '@mui/material/colors';

const BodyPart = ({ item, bodyPart, setBodyPart }) => {
    return (
        <Stack type='button'
            alignItems='center'
            justifyContent='center'
            className='bodyPart-card '
            spacing={0}
            sx={
                {
                    borderTop: bodyPart === item ? '4px solid red' : '',
                    backgroundColor: 'pink',
                    borderBottomLeftRadius: '20px',
                    width: '150px',
                    height: '200px',
                    cursor: 'pointer',
                    gap: '47px',
                    margin: '2px'

                }} z
            onClick={() => {
                setBodyPart(item)
                window.scrollTo({ top: 1800, left: 100, behavior: 'smooth' })
            }}
        >
            <img src={Icon} alt="dumbbell" style={{ width: '40px', height: '40px' }} />
            <Typography fontSize='20px' fontWeight='bold' color="#3A1212" textTransform="capitalize" textAlign="center">{item}</Typography>
        </Stack>
    )
}

export default BodyPart
