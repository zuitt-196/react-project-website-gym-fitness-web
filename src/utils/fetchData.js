
// // export const exerciseOptions = {
// //     method: 'GET',
// //     headers: {
// //         'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com',
// //         'X-RapidAPI-Key': '4664910ffamsh896f6e2f45b458fp1b437djsn61904bdbc7e4',
// //     }
// // };



// // export const fetchData = async (url, options) => {
// //     const res = await fetch(url, options);
// //     const data = await res.json();

// //     return data;
// // };

// import axios from 'axios';

// export const exerciseOption = {
//     method: 'GET',
//     url: 'https://exercisedb.p.rapidapi.com/exercises/bodyPartList',
//     headers: {
//         'X-RapidAPI-Key': process.env.REACT_APP_RAPID_API_KEY,
//         'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com'
//     }
// };

// // export const fetchData = axios.request(exerciseOption).then(function (response) {
// //     return response
// // }).catch(function (error) {
// //     console.error(error);
// // });


// export const fetchData = async (url, options) => {
//     const res = await fetch(url, options);
//     const data = await res.json();

//     return data;
// };




export const exerciseOptions = {
    method: 'GET',
    headers: {
        'X-RapidAPI-Host': 'exercisedb.p.rapidapi.com',
        'X-RapidAPI-Key': process.env.REACT_APP_RAPID_API_KEY,
    },
};


export const fetchData = async (url, options) => {
    const res = await fetch(url, options);
    const data = await res.json();

    return data;
};

